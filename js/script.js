const tableDataList_td = document.querySelectorAll("td");
const month_th = document.getElementById("month");
const months = ["January", "February",
"March", "April", "May",
"June", "July", "August",
"September", "October", "November",
"December"]
const currentDate = new Date();

// Adds event listeners to the arrows
/*
previousMonthArrow = document.querySelector(".previous-month");
nextMonthArrow = document.querySelector(".next-month");

previousMonthArrow.addEventListener('click', () => {
    currentDate.setMonth(currentDate.getMonth()-1);
    setDate();
});
nextMonthArrow.addEventListener('click', () => {
    currentDate.setMonth(currentDate.getMonth()+1);
    setDate();
});
*/
// Sets the days dynamically to the list
// TODO: Refactor to be more efficient
setDate();
function setDate() {
    let startOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth()+1, 0);
    console.log(startOfMonth.getDate());
    console.log(currentDate);
    let day = 1;
    for(let n = startOfMonth.getDay(); day <= startOfMonth.getDate(); n++)
    {
        // checks if the value is the current day and adds a CSS class to it
        if (day == currentDate.getDate()) {
            tableDataList_td[n].classList.add("current-day");
        }
        tableDataList_td[n].innerText = day;
        day++;
    };
    
    // Sets the current month in the header
    month_th.innerText = months[currentDate.getMonth()]; 
}